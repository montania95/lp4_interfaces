/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarealp4;

public class alumno implements persona {

    String nombre;

    public alumno() {
    }

    public alumno(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String getNombre() {
        return nombre;
    }

    @Override
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public void imprimirnombre() {
        System.out.println("El nombre del alumno es:" + nombre);
    }
}
